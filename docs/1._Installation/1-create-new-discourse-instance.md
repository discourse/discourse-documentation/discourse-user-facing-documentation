# Create a new Discourse instance

In order to create a new Discourse instance, we need to do the following:

* To create a new project.
* To open a ticket by specifying which project we have created.

## Create a new project

To create a new project under the App Catalogue cluster (the one that hosts 3rd party applications such as Discourse), you just need to go to <https://webservices-portal.web.cern.ch/app-catalogue>, and specify your `Project name` and your `Project description`, then finally <kbd>Create</kbd>. This will create for you a project under the [App Catalogue](https://app-catalogue.cern.ch) cluster, ready to be used.

Note that as of now, there is no longer relation between the name given for your `Project name` and the hostname of your Discourse instance as it happened in the past for OpenShift 3.

For example, you can create a project called `My Awesome Service`, that eventually will contain a Discourse instance, a Grafana instance, a WordPress instance, etc. all together under the same project (and all of them with their corresponding hostname).

## Open a ticket by specifying which project we have created

To instantiate a Discourse instance, this incurs into creating a new user and a new database to be managed centrally. In order to do so, we need you to open a ticket asking for the creation of a Discourse instance centrally managed.

Please, open a ticket under the following link, providing the name of the project you just have created in the previous step:

* <https://cern.service-now.com/service-portal/?id=sc_cat_item&name=request&se=discourse>

Example of request:

* `Short description`: Creation of a new Discourse instance
* `Detailed description`: I have created the project `My new Project` under App Catalogue. I want to provision a new Discourse instance under this project, with the hostname `my-new-discourse.web.cern.ch`.

!!! info
    Note that at any moment, any user can create its own Discourse instance by using its own database, although **please be aware that this approach is not supported** by the Discourse Service.
