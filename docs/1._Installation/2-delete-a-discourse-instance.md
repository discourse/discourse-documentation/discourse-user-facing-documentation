# Delete a Discourse instance

To delete your Discourse instance, we will go the Web Services portal <https://webservices-portal.web.cern.ch>, find our Discourse instance and delete it from there.

Both the Discourse instance and the project will eventually be deleted in the coming seconds.

!!! warning
    Be sure that indeed, you want to delete the project. This is a **destructive operation** and possibilities of recovering things are practically nonexistent.

If you are using the centrally managed solution provided by the Discourse Service, do not hesitate to mention us that you are going to delete your instance. [Contact us](../99-contact.md).
