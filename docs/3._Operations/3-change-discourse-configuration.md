# Change Discourse configuration

> This document assumes you are familiar with the [edit initial configuration](../2._Configuration/3-edit-initial-configuration.md) procedure.

To change any particular configuration related to Discourse, you can refer to the [Custom Resource Definition (CRD)](https://gitlab.cern.ch/discourse/discourse-operator/-/blob/master/config/crd/bases/discourse.webservices.cern.ch_discourses.yaml) to see what are these possibilities. These properties are based on the [ones offered by the Discourse Team](https://github.com/discourse/discourse/blob/tests-passed/config/discourse_defaults.conf).

At the same time, if you prefer consult them via the OpenShift CLI, you can do it as follows:

```bash
# oc login --server=https://api.app-cat-stg.okd.cern.ch -u <username> # for staging purporses
# oc login --server=https://api.app-catalogue.okd.cern.ch -u <username>

oc explain discourse

# KIND:     Discourse
# VERSION:  discourse.webservices.cern.ch/v1

# DESCRIPTION:
     # Discourse is the Schema for the discourses API

# FIELDS:
# ...

oc explain discourse.spec

# KIND:     Discourse
# VERSION:  discourse.webservices.cern.ch/v1

# RESOURCE: spec <Object>

# DESCRIPTION:
#      Spec defines the desired state of Discourse

# FIELDS:
#    containers   <Object>
#      Resource management for the main deployment.

#    env  <Object>
#      Status defines the observed state of Discourse

#    redis        <Object>
#      Redis resources

#    replicas     <integer>
#      Number of replicas for the Discourse webapp deployment.

#    storage      <string>
#      Storage capacity for the persistent volume claim.

#    visibility   <string>
#      Whether your application is visible from within the CERN network only
#      (Intranet) or exposed outside CERN as well (Internet).

oc explain discourse.spec.env
# KIND:     Discourse
# VERSION:  discourse.webservices.cern.ch/v1

# RESOURCE: env <Object>

# DESCRIPTION:
#      Status defines the observed state of Discourse

# FIELDS:
#    DICOURSE_MAX_LOGSTER_LOGS    <integer>
#      Maximum number of log messages in /logs
# ...

oc explain discourse.spec.env.DICOURSE_MAX_LOGSTER_LOGS
# KIND:     Discourse
# VERSION:  discourse.webservices.cern.ch/v1

# FIELD:    DICOURSE_MAX_LOGSTER_LOGS <integer>

# DESCRIPTION:
#      Maximum number of log messages in /logs
```

Anything under `properties` in the Custom Resource Definition (CRD) can be set in our Custom Resource (CR) under the `spec` element. For example:

The CRD contains the `env` property, which at the same time, contains several properties such us `DISCOURSE_DB_POOL`. In order to modify the `DISCOURSE_DB_POOL` configuration and set a new value into our Discourse instance, we need to edit the initial configuration (we need to edit the Custom Resource), and set the following:

```yaml
apiVersion: discourse.webservices.cern.ch/v1
kind: Discourse
metadata:
  ...
spec:
  env:
    DISCOURSE_DB_BACKUP_PORT: 6609
    DISCOURSE_DB_HOST: xxx
    DISCOURSE_DB_NAME: xxx
    DISCOURSE_DB_PASSWORD: xxx
    DISCOURSE_DB_PORT: 6609
    DISCOURSE_DB_USERNAME: xxx
    DISCOURSE_DEVELOPER_EMAILS: ismael.posada.trobo@cern.ch
    DISCOURSE_HOSTNAME: test-newdiscourse.webtest.cern.ch
    DISCOURSE_DB_POOL: 32 # <-- we add our new configuration
...
```

You can see it graphically explained below. We will just add a new value called `DISCOURSE_DB_POOL` under the `env` property within the Discourse CR:

(_On the left side, you have the Custom Resource (CR). On the right side, you have the Custom Resource Definition (CRD)_)

![image-14.png](../assets/image-14.png)

Finally, <kbd>Save</kbd> and a new re-deployment will take place. Note that these actions require Discourse to redeploy (in english, to reboot), hence it can take some minutes to become effective. Contrary to `spec.storage` and `spec.visibility`, anything under `spec.env` will require a re-deploy of the Discourse instance (i.e., a new pod will be launched with the new configurations).

You can, at any point, see what is the status of the new created pod by expanding `Workloads` (1) and clicking on `Pods` (2). Eventually, a new pod will appear (3) and will carry over the new configuration.

![image-15.png](../assets/image-15.png)

Any missing property from upstream? [Tell us](../99-contact.md)
