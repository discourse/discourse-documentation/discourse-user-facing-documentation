# Increase storage capacity

> This document assumes you are familiar with the [edit initial configuration](../2._Configuration/3-edit-initial-configuration.md) procedure.

To increase the capacity of your Discourse instance, it's as easy as bumping the value of the `spec.storage` element in the Custom Resource (1).

!!! warning
    Note that we can only increase the capacity, and not decrease it.

From:

```yaml
spec:
  ...
  storage: 10Gi
```

To:

```yaml
spec:
  ...
  storage: 20Gi
```

![image-9.png](../assets/image-9.png)

Finally, just <kbd>Save</kbd> and the persitent volume claim will get the capacity set immeditely.

!!! info
    We plead for increasing the capacity in a responsible way. If we initially have set `10Gi`, consider increasing for example to `20Gi`.
    Pay attention to not set a value higher than the quota (by default 100 GiB). To know more about quotas refer to [user docs](https://paas.docs.cern.ch/6._Quota_and_resources/1-project-quota/).
