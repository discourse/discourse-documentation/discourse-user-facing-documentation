# Change visibility of your Discourse instance

> This document assumes you are familiar with the [edit initial configuration](../2._Configuration/3-edit-initial-configuration.md) procedure.

As we have already mentioned under the [configure Discourse](../2._Configuration/1-configure-discourse.md) procedure, and for security reasons, all Discourse instances are only accessible from inside CERN when creating. Below you will find the instructions to make your Discourse instance reachable from outside CERN.

To do so, we just need to edit our CR (Custom Resource), changing the value of the `spec.visibility` element to `Internet` (1).

From:

```yaml
spec:
  ...
  visibility: Intranet
```

To:

```yaml
spec:
  ...
  visibility: Internet
```

![image-8.png](../assets/image-8.png)

Finally, we click on <kbd>Save</kbd> or `Ctrl + S` (2) and the CR will get automatically updated.

Your forum will be accessible from outside CERN in seconds.
