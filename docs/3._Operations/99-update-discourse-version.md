# Update Discourse version

Discourse instance administrators might occasionally receive emails prompting them to upgrade their software.

However, within the CERN infrastructure, these emails and the upgrade actions they suggest are unnecessary. This is because versions are centrally managed, and new releases are automatically pushed to all instances by our dedicated Discourse cluster administrators.

Our cluster administrators strive to stay current with the latest versions. As a result, whenever a new version of Discourse is released by the Discourse Team, it's implemented across all our instances as soon as possible.

## Can I disable emails about Discourse updates if there's nothing for me to do?

Absolutely! Since our Discourse instances are centrally managed, administrators don't need to take any action for updates. Here's how to stop receiving those email:

1. Visit your admin panel: `https://<my-instance>.web.cern.ch/admin/site_settings/category/required`
2. In the "type to filter" field, enter `version checks`.
3. Uncheck the `version checks` setting.
4. Repeat steps 2 and 3, but this time enter the `new version emails` setting instead.

By disabling these settings, you'll no longer receive update notifications for upgrades in Discourse.

## How will I know about upcoming Discourse deployments?

New versions of Discourse are typically deployed at CERN within a one-week window. We announce any planned maintenance or interventions on the [Service Status Board](https://cern.service-now.com/service-portal?id=service_status_board).

If you'd like more information or if a new update might cause any inconvenience, feel free to [contact us](../99-contact.md).
