# Admin login blocked

It might happens that, accidentally, you have configured the Discourse OAuth2 plugin, as explained in [Configure CERN SSO](../2._Configuration/2-configure-cern-sso.md), to allow users to authenticate with their CERN/Federated accounts, and suddenly, you are not able to login because of some missing or wrong configuration was set.

We can re-connect to the Discourse instance as **administrators** to the following url:

- `https://<my-instance>/u/admin-login` (e.g., `https://my-discourse.web.cern.ch/u/admin-login`)

!!! info
    Note that to connect as administrator, you should have been granted this permission in the past by one of the existing administrators. Otherwise, only people under the `DISCOURSE_DEVELOPERS_EMAILS` setting are able to perform this action.

ffi: <https://meta.discourse.org/t/admin-login-blocked/124142>
