# Edit a user setting for all Discourse users

It might happens that at some point, you want to set a setting for all Discourse users in your forum. Normally, when you enable one setting affecting users, some of them affect for new users, but existing ones won't have populated such setting.

For example, if we want to default all existing users to mailing list mode, we can do something like the following from the `webapp` container terminal:

```bash
discourse@webapp-xxx-7f49bfd87c-q5sjs:/var/www/discourse$ script/rails c
Loading production environment (Rails 7.0.2.4)
irb(main):001:0> UserOption.update_all(mailing_list_mode: true)
```

And that's it. All users have now the mailing list setting set to `true`.

More actions that could be executed can be found under <https://meta.discourse.org/t/edit-a-user-setting-for-all-discourse-users/25162>
