# Configure your Discourse instance

So your Discourse instance is ready, and it's time to set it up. We will cover the basics on this documentation, then it's up to the user to continue acquiring knowledge by pointing to the official documentation under <https://meta.discourse.org>.

!!! info
    For security reasons, and until you get acquainted with your Discourse instance, every new Discourse instance is only accessible from inside CERN. You will learn in this documentation how to open your Community outside CERN.

## First steps

You access your brand new Discourse instance under `https://<my-instance>`, and the first picture you get is the following:

![image-6.png](../assets/image-6.png)

Congratulations. You are two steps behind of becoming the new Discourse administrator of your forum.

In this window, you just need to click on <kbd>Register</kbd>

Next window, we will configure our first **local account**. This local account will allow us to set up our first configurations. It will use your `email` account since you are the owner of the Discourse instance. The account `svc.discourse@cern.ch` is also provided by the Discourse infrastructure team for infrastructure matters.

![image-7.png](../assets/image-7.png)

It's preferrable (and not mandatory) to set up the following:

* In the Email combo box, we will find our CERN email account. Select it.
* In the Username field, it’s preferable to put our CERN username, so this will be the one we will use once we have CERN SSO properly configured.
* In the Password field, this password **MUST** be totally different to the one used at CERN, fitting the rule of having at least 15 characters.

Note that local accounts set in here, they could be eventually overriden whether we [configure CERN SSO](../2._Configuration/2-configure-cern-sso.md) for our Discourse instance, so that they will be bypassed and your local account will be merged with the one that comes from CERN SSO.

Once we register, we will get an email to confirm the creation of the local account.

We will have the opportunity to configure some visual and operational aspects on this screen. At any moment you can get back to them going to `https://<your_instance>/wizard`.

Last but not least, we recommend you to have a look to the official [User Guide](https://meta.discourse.org/t/discourse-new-user-guide/96331) provided by the Discourse Team.

Voilà, your Discourse instance is ready!
