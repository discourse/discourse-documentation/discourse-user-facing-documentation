# Allow external users to login in your Discourse instance

In order to allow external users to login in your Discourse instance, the procedure is as follows:

Go to <https://application-portal.web.cern.ch/>.
Search for the application registration related to your instance. They are in the form `webframeworks-app-catalogue-<instance>` (e.g., `webframeworks-app-catalogue-drupal-community`):

- Or you can just navigate to `https://application-portal.web.cern.ch/manage/webframeworks-app-catalogue-<instance>` (e.g., `https://application-portal.web.cern.ch/manage/webframeworks-app-catalogue-drupal-community`).

Switch to the tab `Roles`.

![application-registration-roles-tab.png](../assets/application-registration-roles-tab.png)

Edit the `Default Allowed Users` role, by clicking the orange button `Edit role details`.

![application-registration-roles-edit-roles.png](../assets/application-registration-roles-edit-roles.png)

In the popup window, move the `Minimum Level of Assurance` (1) at your best convenience:

- If you want people with social accounts (Gmail, GitHub, Facebook, etc.) to login, move the slider to `Social Account`.
- To set `Any Account` is discouraged for security reasons, although we understand that sometimes there is no other option. This scenario contemplates all accounts.

![application-registration-roles-set-loa.png](../assets/application-registration-roles-set-loa.png)

Finally, <kbd>submit</kbd> the change (2).

As of this moment, accounts that belong to the level of Assurance will be able to login into your Discourse instance.

n.b.: for further information about setting roles, please visit the [Authorization Service documentation](https://auth.docs.cern.ch/)
