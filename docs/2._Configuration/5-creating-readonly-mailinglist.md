# Creating a read-only mailing list mirror

> original posts:
>
> - <https://meta.discourse.org/t/creating-a-read-only-mailing-list-mirror/77990>

This guide describes how you can set up your Discourse instance for creating a **read-only mirror** of an e-group mailing list. Thus, everytime a user sends an email to `mymailinglist@cern.ch`, a post will appear in a specific category of your forum.

The general steps are:

- To create a service account.
- To disable forward to own the account.
- To add the service account to an e-group.
- To open a request with Discourse service to configure the MSGraph Polling Plugin and to fetch the refresh token.
- To configure your Discourse forum accordingly.
- To configure a category for mirroring.

## Create a service account

A service account is needed. This service account will have an email address (e.g. `svc-account@cern.ch`).

The steps are:

- Create a service account [here](https://account.cern.ch/account/Management/NewAccount.aspx).

 It takes some minutes to be available.

## Disable forward on your service account

By default, when you create a service account, it comes with the `forwarded` option enabled. This means that every mail sent to this service account, will land into your primary account's email mailbox. This we don't need it, hence we must disable it.

Go to <https://account.cern.ch/account/Management/MyAccounts.aspx> and disable it.

## To add the service account to an e-group

As stated in <https://meta.discourse.org/t/creating-a-read-only-mailing-list-mirror/77990#subscribe-to-mailing-list-3>, to subscribe your account to the mailing list you just need to add the corresponding service account to the e-group.

## Open a request to the Discourse Team to configure the MS Graph Polling Plugin

!!! info

    As of October 2023, the use of basic authentication (username+password) for polling is deprecated by Microsoft. Thus, until Discourse team releases a new way of authenticating via token (and not via username/password), users will have to use the [MS Graph Plugin](https://github.com/CERN/msgraph-poll-discourse-plugin), created by our colleagues from the Email Service in collaboration with the Discourse Service.
   
    ffi: <https://learn.microsoft.com/en-us/exchange/clients-and-mobile-in-exchange-online/deprecation-of-basic-authentication-exchange-online>

To configure the MS Graph polling plugin, open a ticket under <https://cern.service-now.com/service-portal?id=sc_cat_item&name=request&se=discourse>, and tell us about your instance. We will ask you some information to complete this step, such as the service account username used and password.

## Configure your Discourse forum accordingly

So at this point, you should have a service account created, with the forwarding option disabled. Now it's time to configure your Discourse instance.

For that, we just need to go to `Email Settings` (e.g., `https://<my-forum>/admin/site_settings/category/email`), and set up the following values:

- `email_in`: checked (optional)
- `manual polling enabled`: unchecked
- `pop3 polling enabled`: unchecked (will use MS Graph Polling Plugin instead because of the basic authentication limitation)

Under the MSGraph Polling Plugin (filled by Discourse administrators when submitting the request):

- `msgraph polling enabled`: checked
- `msgraph polling mailbox`: svc-account@cern.ch
- `msgraph polling client id`: <uuid>
- `msgraph polling tenant id`: <uuid>
- `msgraph polling oauth2 refresh token`: <obtained token as per instructions in the plugin>
- `msgraph polling login endpoint`: https://login.microsoftonline.com
- `msgraph polling graph endpoint`: https://graph.microsoft.com/v1.0

Owner of the service account can navigate to <https://outlook.office365.com/owa/cern.ch> (login with the service account user/pwd) to see messages arriving. **Messages will be auto-purged from Exchange Online once delivered**, so no need for an extra setting for that.

## Configure a category for mirroring

To configure a category or multiple categories for mirroring your mailing list, the following procedure applies:

- <https://meta.discourse.org/t/create-a-read-only-mailing-list-mirror/77990#category-2>

Make sure to specify your e-group email address in the `Custom incoming email address` (e.g., `my-egroup@cern.ch`), and most important, make sure that the service account belongs to the specific e-group.

And that's it. Every message send to the mailing list, should start appearing in the desired category.

---

You can optionally complement your configuration adding the [setting up reply-via-mail](./4-setup-reply-by-mail.md) feature.
Configuration is mainly done in this document, but the need of adding `reply by mail enabled` and `reply by email address` settings are needed to make the `reply-by-mail` feature to work.
