# Configure CERN SSO

The [Discourse OAuth2 Basic plugin](https://meta.discourse.org/t/oauth2-basic-support/33879) allow users to login by using their CERN account, as well as any federated account such as GMail, Outlook, etc., and it comes installed by default in our Discourse instances.

When a project is created under the new OpenShift 4 infrastructure, and concretely, under the [App Catalogue](https://app-catalogue.cern.ch) cluster, it automatically registers an application registration under the [Application Portal](https://application-portal.web.cern.ch) in the form of `webframeworks-app-catalogue-PROJECT_NAME`. Thus, if we create on a project named `my-project`, the application registration will have the name `webframeworks-app-catalogue-my-project`. For the staging cluster, under <https://app-cat-stg.cern.ch>, this will be in the form of `webframeworks-app-cat-stg-PROJECT_NAME`.

This application registration serves dual purposes:

- It controls the [lifecycle of the OKD4 project](https://paas.docs.cern.ch/#lifecyle-of-okd4-applications), common to all OpenShift 4 instances.
- It provides the necessary information for your application to be able to authorize users to login into your Discourse instance.

## Creation of the oidc-client-secret

As a consequence of the creation of this project, a secret under our project called `oidc-client-secret` is automatically created, containing among others the `clientID` and the `clientSecret`, related to this `webframeworks-app-cat-stg-PROJECT_NAME` application registration and **needed** for our Discourse OAuth2 plugin configuration as you will see next. You can check this secret by going to the [App catalogue cluster](https://app-catalogue.cern.ch) ([App catalogue staging cluster](https://app-cat-stg.cern.ch) if needed), selecting your project (1), expand `Workloads` (2) and clicking on `Secrets` (3). On the right side of the screen, you will see a secret resource called `oidc-client-secret`.

![image-4.png](../assets/image-4.png)

Opening this `oidc-client-secret` secret, you will observe that it contains the data we need to carry on. Reveal values (1) and copy both `clientID` and `clientSecret` when needed. They will be needed in the following point:

![image-5.png](../assets/image-5.png)

## Configure Discourse OAuth2 plugin

!!! warning
    Prior to proceed enabling CERN SSO, be sure that the `force_https` setting is enabled under `https://<your_instance>/admin/site_settings/category/security?filter=force%20https`.
    This is important, otherwise your authentication system won't work.

To configure the Oauth2 plugin under your Discourse instance, go to the settings of your Discourse instance:

![image-1.png](../assets/image-1.png)

And navigate to the `Login` tab:

![image-2.png](../assets/image-2.png)

At the bottom of the page, we will find the settings related to the `OAuth2` plugin.

We need to set the following settings:

![image-3.png](../assets/image-3.png)

settings the following values:

- `oauth2 enabled`: enabled.
- `oauth2 client id`: `<clientId>` value taken from the `oidc-client-secret` secret above.
- `oauth2 client secret`: `<clientSecret>` value from the `oidc-client-secret` secret above.
- `oauth2 authorize url`: `https://auth.cern.ch/auth/realms/cern/protocol/openid-connect/auth`
- `oauth2 token url`: `https://auth.cern.ch/auth/realms/cern/protocol/openid-connect/token`
- `oauth2 user json url`: `https://auth.cern.ch/auth/realms/cern/protocol/openid-connect/userinfo?oauth_token=:token`
- `oauth2 json user id path`: `cern_upn`
- `oauth2 json username path`: `cern_upn`
- `oauth2 json name path`: `name`
- `oauth2 json email path`: `email`
- `oauth2 email verified`: checked
- `oauth2 scope`: `openid profile email`

!!! info
    A tip to set up this configuration is to open a browser, set these values, and then open a new incognito window to validate that we certainly can login into the Discourse instance without any problem.
    In case the configuration was wrongly set, and we accidentally cannot login into the Discourse instance, there are ways to get into the forum as administrator bypassing the Oauth2 plugin. See troubleshooting part of this documentation for further information.

Last but not least, the `redirectURI` will be automatically generated based on the hostname (e.g., `my-forum.web.cern.ch`) you have selected for your Discourse instance, and will be self-configured when the Discourse instance is provisioned thanks to the `OIDCReturnURI` operator deployed for you. Bottom line is that no further actions are needed from the user to configure a `redirect uri` for your Discourse instance.

### Extra configuration for the Discourse OAuth2 plugin

It might happen that we want to avoid users modifying their `names`, their `fullnames` and just to preserve those provided by the Authorization system, or to automatically register users into our forum without confirmation on their side.

To do that, in the same Login settings page as explained above, we have to enable the following settings (they can be enabled separately, and they do not depend each other):

- `auth skip create confirm`: enabled.
- `auth overrides email`: enabled.
- `auth overrides username`: enabled.
- `auth overrides name`: enabled.

And that's it. At this point, you should be able to login into your Discourse instance by using the Discourse OAuth2 plugin.

For further information about more possibilities of configuration regarding OAuth2, check upstream under <https://meta.discourse.org>.
