# Setup Discourse Email integration (reply-by-mail)

> original posts:
>
> - <https://meta.discourse.org/t/set-up-reply-via-email-support/14003>
> - <https://meta.discourse.org/t/configuring-reply-via-email/42026>

So you'd like to set up reply by mail for your Discourse forum. To configure this at CERN, we need to setup the following:

- To create a service account.
- To disable forward to own the account
- To open a request with Discourse service to configure the MSGraph Polling Plugin and to fetch the refresh token.
- To configure your Discourse forum accordingly.
- To start topic by mail.

## Create a service account

It's needed to create a service account. This service account will have an email in the form of `svc-account@cern.ch`. To do that, go to <https://account.cern.ch/account/Management/NewAccount.aspx> and create a new one. It takes some minutes to be available.

It will be used to send mails in the form of `svc-account+<category-name>@cern.ch`, when using subaddressing. 

## Disable forward on your service account

By default, when you create a service account, it comes with the `forwarded` option enabled. This means that every mail sent to this service account, will land into your primary account's email mailbox. This we don't need it, hence we must disable it.

## Open a request with Discourse Service

Next step, it is to open a ticket with the Discourse Service at <https://cern.service-now.com/service-portal?id=sc_cat_item&name=request&se=discourse>, asking for to setup the previous service account to the use `+` sub-addressing for Discourse.

## Configure your Discourse forum accordingly

So at this point, you should have a service account created, with the forwarding option disabled. Now it's time to configure your Discourse instance.

For that, we just need to go to `Email Settings` (e.g., `https://<my-forum>/admin/site_settings/category/email`), and set up the following values as shown below:

![image-16.png](../assets/image-16.png)

Most important values to fill are (needs to be filled by you):

- `reply by email enabled`: checked
- `reply by email address`: must be `service_account+%{reply_key}@cern.ch` (e.g., `myforumsa+%{reply_key}@cern.ch`).
- `email_in`: checked (optional)
- `manual polling enabled`: unchecked
- `pop3 polling enabled`: unchecked

And under the MSGraph Polling Plugin (filled by Discourse administrators when submitting the request):

- `msgraph polling enabled`: checked
- `msgraph polling mailbox`: svc-account@cern.ch
- `msgraph polling client id`: <uuid>
- `msgraph polling tenant id`: <uuid>
- `msgraph polling oauth2 refresh token`: <obtained token as per instructions in the plugin>
- `msgraph polling login endpoint`: https://login.microsoftonline.com
- `msgraph polling graph endpoint`: https://graph.microsoft.com/v1.0

User can navigate to <https://outlook.office365.com/owa/cern.ch> (login with the service account user/pwd) to see messages arriving. **Messages will be auto-purged from Exchange Online once delivered**, so no need for an extra setting for that.

## Start topic via mail

> original post: <https://meta.discourse.org/t/start-a-new-topic-via-email/62977>

Once everything is configured in Discourse, we are ready to start sending mails and to appear under our Discourse instance. We need to configure **each** of the categories/groups we want them to receive mails.

In the settings of a category, set the `Custom incoming email address` like `service_account+category_name@cern.ch` (e.g., `myforumsa+detectors@cern.ch`), as follows.

![image-17.png](../assets/image-17.png)

And it's done. Now if you try to send an email with **a logged account in your Discourse forum** to `myforumsa+detectors@cern.ch`, the message will appear as a topic in the category `Detector`.

!!! info
    The **topic-email address** (i.e., `myforumsa+detectors@cern.ch`), is only visible by the administrators of the forum.
    Therefore it might be convenient to display the mail in the "About category" post which is partially visible in the category overview:
    ![image-18.png](../assets/image-18.png)

## Misc

Should I configure `Categories` or `Groups`?

Have a look to <https://meta.discourse.org/t/start-a-new-topic-via-email/62977> > `Category vs Group`.

<small>Special thanks to Artur Lobanov for bringing us this awesome step-by-step configuration.</small>
