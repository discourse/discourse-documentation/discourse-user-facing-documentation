# Edit initial configuration

Users have the possibility of editing the initial manifest in order to provide new configurations, such as settings visibility for our Discourse instance, or to provide an specific configuration via environment variable.

This needs to be done through the YAML manifest as shown below.

First, select the project to act upon (1). Then, under the `Administrator` environment (2), expand `Operators` (3) and click on Installed Operators (4). Click on the `Discourse Operator` operator (5).

![image-10.png](../assets/image-10.png)

In the new window, do not get confused with the `YAML` tab. **Make sure you click** on the Discourse tab (1):

![image-11.png](../assets/image-11.png)

Click on the Discourse instance you want to change the configuration:

![image-12.png](../assets/image-12.png)

In the new screen, **we can now click on** the `YAML` tab (1):

![image-13.png](../assets/image-13.png)

In this YAML manifest, we will be able to edit everything contained under `spec`.

Note that this procedure is widely used when there is the need of configuring anything for our Discourse instance, so let's get acquainted with it!
