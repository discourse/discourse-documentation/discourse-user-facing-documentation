# Getting Help

## Report an issue

To report an issue or ask for assistance, contact us at [Service Desk - Discourse Service](https://cern.service-now.com/service-portal/?id=service_element&name=discourse)

## General questions

For more general questions related to the application, we are also reachable under:

* [~Discourse](https://mattermost.web.cern.ch/it-dep/channels/discourse) channel in Mattermost.
* Central IT Discourse forum at the [Openshift category](https://discourse.web.cern.ch/c/web-hosting-development/openshift).

## Discourse catalogue at CERN

* You can visit what are the Discourse instances deployed at CERN under the [Forum Catalogue](https://forum-catalogue.web.cern.ch/)

## Upstream documentation

For official documentation, refer to:

* [Discourse Meta](https://meta.discourse.org/)

## Upstream blog

Discourse official blog is hosted under:

* <https://blog.discourse.org/>
