# Discourse user-facing documentation

This pages contains useful information about how to install and configure a Discourse instance. They are intended for user's usage.

This documentation is deployed under <https://discourse.docs.cern.ch>.
